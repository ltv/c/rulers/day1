const express = require('express');
const app = express();
const port = 8080;

//Route /
app.get('/', (request, response) => response.send('Welcome to my first nodejs app'));

// Route /hello
app.get('/hello', (request, response) => {
  let name = request.query['name']
  name = name || 'guy'
  response.send(`Hello ${name}!`)
});


// Start app
app.listen(port, () => console.log(`App is running on port ${port}`))
